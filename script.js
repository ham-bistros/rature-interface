////// FONCTION D'ENREGISTREMENT //////
function download(data, filename, type) {
    var file = new Blob([data], {type: type});
    if (window.navigator.msSaveOrOpenBlob) // IE10+
        window.navigator.msSaveOrOpenBlob(file, filename);
    else { // Others
        var a = document.createElement("a"),
                url = URL.createObjectURL(file);
        a.href = url;
        a.download = filename;
        document.body.appendChild(a);
        a.click();
        setTimeout(function() {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);
        }, 0);
    }
}

////// VARIABLES //////
var ecrit = document.getElementById('texte-tapé');
var affiche = document.getElementById('texte-affiché');

// tous les caractères dont le nom ne doit pas s'afficher au milieu du texte
var invisibleChars = ['Shift', 'CapsLock', 'BrightnessDown', 'BrightnessUp', 'Process', 'Tab', 'Enter', 'Control', 'Alt', 'Option', 'Escape', 'Fn', 'Backspace', 'F1', 'F2', 'F3', 'F4', 'F5', 'F6', 'F7', 'F8', 'F9', 'F10', 'F11', 'F12', 'Pause', 'PageUp', 'PageDown', 'End', 'Home', 'ArrowLeft', 'ArrowUp', 'ArrowRight', 'ArrowDown', 'PrintScreen', 'Insert', 'Delete', 'Meta', 'OS', 'ContextMenu', 'NumLock', 'ScrollLock', 'AudioVolumeMute', 'AudioVolumeDown', 'AudioVolumeUp', 'LauchMediaPlayer', 'LauchApplication1', 'LauchApplication2', 'AltGraph', 'Unidentified', 'Hyper', 'SingleCandidate', 'PreviousCandidate', 'NextCandidate', 'NonConvert', 'Compose', 'Multi', 'Dead'];

var index = 0;
var nbrSuppr = 0; //// pour "supprimer" plusieurs caractères d'affilée

////// CÉPARTI //////
var key = '';
ecrit.addEventListener('input', function() {
  ecrit.onkeydown = function() {
    key = event.key;
  };

  input = ecrit.value.slice(-1);
  if (input == '\n') {
    affiche.insertAdjacentHTML('beforeend', '</br>');
    nbrSuppr = 1; // reset la suppression d'affilée
  } else if (key == 'Backspace') {
    if (index != 0) {
      aSup = document.getElementById(`${String(index - nbrSuppr)}`); // sélectionne l'id de l'élément à supprimer
      if (!aSup.classList.contains('rature')) {
        aSup.classList.add('rature');
      }
      nbrSuppr += 1;
    }
  } else if (!invisibleChars.includes(key) || key == 'Process') {
    // le 'Process' permet de capturer les capitales accentuées
    if (index === 0) {
      affiche.innerHTML = '';
    }

    ajout = document.createElement('span');
    ajout.setAttribute('id', index)
    console.log(input);
    ajout.innerHTML = input;
    affiche.appendChild(ajout);

    index += 1;
    nbrSuppr = 1; // reset la suppression d'affilée
  }
});

// bouton pour ENREGISTRER
var save = document.getElementById('save');
save.addEventListener('click', function() {
  // ranger les ratures consécutives dans les mêmes spans
  var ratures = [...document.getElementsByClassName('rature')];
  ratures.forEach((rat, i) => {
    if (i > 0) {
      let precId = parseInt(ratures[i-1].id);
      if (rat.id == precId + 1) {
        rat.insertAdjacentHTML('afterbegin', ratures[i-1].innerHTML);
         ratures[i-1].parentNode.removeChild(ratures[i-1]);
      }
    }
  });

  // enlève les <spans> inutiles (pas ratures) et les <br>
  const reg1 = /<span id="[0-9]*?">(.)<\/span>/g;
  const reg2 = /<span id="[0-9]*?"/g;
  contenu = affiche.innerHTML;
  contenuPropre = contenu.replace(reg1, "$1");
  contenuPropre = contenuPropre.replace(reg2, '<span');
  contenuPropre = contenuPropre.replace('<br>', '\n');

  download(contenuPropre, 'rature.txt', 'text');
});
